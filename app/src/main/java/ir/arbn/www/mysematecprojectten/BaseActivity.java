package ir.arbn.www.mysematecprojectten;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by A.R.B.N on 2/14/2018.
 */

public class BaseActivity extends AppCompatActivity {
    public Context mContext = this;
    public Activity mActivity = this;
}
