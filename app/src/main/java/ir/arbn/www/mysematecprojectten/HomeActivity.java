package ir.arbn.www.mysematecprojectten;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends BaseActivity {
    MyImageView myImage;
    TextView txt1;
    MyTextView txt3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        myImage = findViewById(R.id.myImage);
        txt1 = findViewById(R.id.txt1);
        txt3 = findViewById(R.id.txt3);
        myImage.loadURL("https://cdn.myfonts.net/s/aw/720x360/462/0/236940.png");
        Typeface irSans = Typeface.createFromAsset(getAssets(), "iransans.ttf");
        txt1.setTypeface(irSans);
        txt3.setBold = true;
        txt3.setTF();
    }
}