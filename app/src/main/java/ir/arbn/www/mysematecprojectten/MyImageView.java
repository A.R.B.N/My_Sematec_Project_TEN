package ir.arbn.www.mysematecprojectten;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

/**
 * Created by A.R.B.N on 2/14/2018.
 */

public class MyImageView extends AppCompatImageView {
    private Context context;

    public MyImageView(Context context) {
        super(context);
        this.context = context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void loadURL(String url) {
        Glide.with(context).load(url).into(this);
    }
}
