package ir.arbn.www.mysematecprojectten;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by A.R.B.N on 2/14/2018.
 */

public class MyTextView extends AppCompatTextView {
    private Context context;
    public boolean setBold = false;

    public MyTextView(Context context) {
        super(context);
        this.context = context;
        setTF();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray attrsArrey = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
        setBold = attrsArrey.getBoolean(R.styleable.MyTextView_isBold,false);

        setTF();
    }

    public void setTF() {
        String typeAddress = "iransans.ttf";
        if (setBold) typeAddress = "iransans_bold.ttf";
        Typeface sans = Typeface.createFromAsset(context.getAssets(), typeAddress);
        this.setTypeface(sans);
    }
}
